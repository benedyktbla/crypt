
#include "stegano.h"

//#include <iostream>
namespace crypt {
namespace stegano {

void stegano(std::istream &carrier, std::string payload, std::ostream &msg,
        unsigned offset) {
    char pc, cc;
    for(unsigned i = 0; i < offset && carrier.get(cc); i++) {
        msg.put(cc);
    }
    for(const char *pcp = payload.c_str();; pcp++) {
        pc = *pcp;
        for(int i = 0; i < 8; i++, pc >>= 1) {
            if(!carrier.get(cc)) {
                throw stegano_error("carrier file too short");
            }
            msg.put((cc & ~1) | (pc & 1));
        }
        if(*pcp == 0) {
            break;
        }
    }
    while(carrier.get(cc)) {
        //std::cerr << int(cc) << std::endl;
        msg.put(cc);
    }
}

void unstegano(std::istream &msg, std::ostream &os, unsigned offset) {
    msg.seekg(offset, std::ios::cur);
    char mc, pc = 0;
    int bit = 0;
    while((mc = msg.get()) != EOF) {
        pc |= (mc & 1) << (bit++);
        if(bit == 8) {
            if(pc == 0) {
                break;
            }
            os.put(pc);
            bit = 0;
            pc = 0;
        }
    }
}

}
}
