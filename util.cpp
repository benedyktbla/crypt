
#include <random>

#include "util.h"

namespace crypt {
namespace util {

Block::Block() : size_(0), data_(nullptr) { }

Block::~Block() {
    reset();
}

void Block::set(unsigned size) {
    reset();
    size_ = size;
    data_ = new unsigned char[size_];
}

void Block::reset() {
    size_ = 0;
    if(data_) {
        delete []data_;
        data_ = nullptr;
    }
}

unsigned Block::size() {
    return size_;
}

unsigned char *Block::data() {
    return data_;
}

InBlockIterator::InBlockIterator() : is(nullptr) { }

InBlockIterator::~InBlockIterator() { }

unsigned char InBlockIterator::read_byte() {
    return static_cast<unsigned char>(is->get());
}

void InBlockIterator::set(unsigned block_size, std::istream &is) {
    reset();
    this->is = &is;
    block.set(block_size);
}

void InBlockIterator::reset() {
    is = nullptr;
    block.reset();
}

InBlockIterator::operator bool() {
    return is;
}

const unsigned char *InBlockIterator::operator*() {
    return block.data();
}

InBlockIterator &InBlockIterator::operator++() {
    is->read(reinterpret_cast<char *>(block.data()), block.size());
    if(is->gcount() == 0) {
        reset();
    } else if(is->gcount() < block.size()) {
        reset();
        throw error("input not block-sized");
    }
    return *this;
}

OutBlockIterator::OutBlockIterator() : os(nullptr) { }

OutBlockIterator::~OutBlockIterator() { }

void OutBlockIterator::write_byte(unsigned char c) {
    os->put(static_cast<char>(c));
}

void OutBlockIterator::set(unsigned block_size, std::ostream &os) {
    reset();
    this->os = &os;
    block.set(block_size);
}

void OutBlockIterator::reset() {
    os = nullptr;
    block.reset();
}

unsigned char *OutBlockIterator::operator*() {
    return block.data();
}

OutBlockIterator &OutBlockIterator::operator++() {
    os->write(reinterpret_cast<char *>(block.data()), block.size());
    return *this;
}

std::random_device random_device;

int random(int b) {
    std::uniform_int_distribution<> u(0, b - 1);
    return u(random_device);
}

}
}

