
#ifndef HILL_H
#define HILL_H

#include "crypt.h"
#include "zn.h"
#include "linalg.h"
#include "util.h"

namespace crypt {

template<unsigned n = 2>
class HillCipher : public Cipher {
public:
    typedef linalg::Matrix<zn::Z<256>, n, n> Matrix;
    typedef linalg::Matrix<zn::Z<256>, 1, n> Vector;

    struct Key {
        Matrix a;
        Vector b;
    };

    HillCipher() : Cipher(n) { }

    void set_ekey(const Key &key) {
        try {
            inv(key.a);
            this->key = key;
        } catch(std::domain_error &) {
            throw Cipher::key_error("Bad key");
        }
    }

    void set_dkey(const Key &key) {
        try {
            compute_dkey(key);
        } catch(std::domain_error &) {
            throw Cipher::key_error("Bad key");
        }
    }

protected:

    Key key;

    void compute_dkey(const Key &ekey) {
        key.a = inv(ekey.a);
        key.b = -ekey.b * key.a;
    }

    void encrypt_block(const unsigned char *inblk, unsigned char *outblk) override {
        Vector v(inblk);
        dump(v * key.a + key.b, outblk);
    }

    void decrypt_block(const unsigned char *inblk, unsigned char *outblk) override {
        encrypt_block(inblk, outblk);
    }

};

using AffinCipher = HillCipher<1>;

}

#endif
