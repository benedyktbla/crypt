
#ifndef UTIL_H
#define UTIL_H

#include <string>
#include <istream>
#include <ostream>
#include <stdexcept>

namespace crypt {
namespace util {

template<int i> struct dummy { dummy(int) { } };

using std::to_string;

template<typename T>
std::string to_string(T v) {
    return std::string(v);
}

template<bool condition>
struct yesno_enable {
    static const bool yes = true;
};

template<>
struct yesno_enable<false> {
    static const bool no = false;
};

template<typename T>
struct marker { };

class Block {
private:
    unsigned size_;
    unsigned char *data_;
public:
    Block();
    Block(const Block &) = delete;
    ~Block();
    Block &operator=(const Block &) = delete;
    void set(unsigned size);
    void reset();
    unsigned size();
    unsigned char *data();
    // TODO operator[]
};

class InBlockIterator {
protected:
    std::istream *is;
    Block block;
    unsigned char read_byte();
public:
    class error : public std::logic_error { using std::logic_error::logic_error; };
    InBlockIterator();
    InBlockIterator(const InBlockIterator &) = delete;
    virtual ~InBlockIterator();
    InBlockIterator &operator=(const InBlockIterator &) = delete;
    virtual void set(unsigned block_size, std::istream &is);
    virtual void reset();
    explicit operator bool();
    const unsigned char *operator*();
    virtual InBlockIterator &operator++(); //prefix
};

class OutBlockIterator {
protected:
    std::ostream *os;
    Block block;
    void write_byte(unsigned char c);
public:
    OutBlockIterator();
    OutBlockIterator(const OutBlockIterator &) = delete;
    virtual ~OutBlockIterator();
    OutBlockIterator &operator=(const OutBlockIterator &) = delete;
    virtual void set(unsigned block_size, std::ostream &os);
    virtual void reset();
    unsigned char *operator*();
    virtual OutBlockIterator &operator++(); //prefix
};

int random(int b);

}
}

#endif

