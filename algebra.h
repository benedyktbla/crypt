
#ifndef ALGEBRA_H
#define ALGEBRA_H

namespace crypt {
namespace algebra {

// CRTP
template<typename D>
class Base {
protected:

    typedef D Derived;

    Derived &derived() {
        return static_cast<Derived &>(*this);
    }

    const Derived &derived() const {
        return static_cast<const Derived &>(*this);
    }
};


template<typename B>
class Equatable : public B {
public:

    friend bool operator==(const Equatable &a, const Equatable &b) {
        return !(a.derived() != b.derived());
    }

    friend bool operator!=(const Equatable &a, const Equatable &b) {
        return !(a.derived() == b.derived());
    }
};


template<typename B>
class Addition : public B {
public:

    typename B::Derived &operator+=(const Addition &a) {
        return B::derived() = B::derived() + a.derived();
    }

    friend typename B::Derived operator+(const Addition &a, const Addition &b) {
        return typename B::Derived(a.derived()) += b.derived();
    }

    typename B::Derived &doubled() const {
        return B::derived() + B::derived();
    }
};


template<typename B>
class Multiplication : public B {
public:

    typename B::Derived &operator*=(const Multiplication &a) {
        return B::derived() = B::derived() * a.derived();
    }

    friend typename B::Derived operator*(const Multiplication &a, const Multiplication &b) {
        return typename B::Derived(a.derived()) += b.derived();
    }

    typename B::Derived &square() const {
        return B::derived() * B::derived();
    }
};


template<typename B>
class AddAssociativity : public B { };


template<typename B>
class MulAssociativity : public B { };


template<typename B>
class HasZero : public B { };

template<typename T>
T zero(const T &a) {
    return a.zero();
}

template<typename T>
T zero(const T &, ...) {
    return 0; // implicit conversion
}


template<typename B>
class HasOne : public B { };

template<typename T>
T one(const T &a) {
    return a.one();
}

template<typename T>
T one(const T &, ...) {
    return 1; // implicit conversion
}


template<typename B>
class AddCommutativity : public B { };


template<typename B>
class MulCommutativity : public B { };


template<typename B>
class AddInverse : public B {
public:

    typename B::Derived operator-() const {
        return B::derived().zero() - B::derived();
    }

    typename B::Derived &operator-=(const AddInverse &a) {
        return B::derived() += -a.derived();
    }

    friend typename B::Derived operator-(const AddInverse &a, const AddInverse &b) {
        return typename B::Derived(a.derived()) -= b.derived();
    }
};


template<typename B>
class MulInverse : public B {
public:

    typename B::Derived inv() const {
        return B::derived().one() / B::derived();
    }

    typename B::Derived &operator/=(const MulInverse &a) {
        return B::derived() *= a.derived().inv();
    }

    friend typename B::Derived operator/(const MulInverse &a, const MulInverse &b) {
        return typename B::Derived(a.derived()) /= b.derived();
    }
};


template<typename B>
class Distributivity : public B { };


template<typename B>
class NoZeroDivisors : public B { };


template<typename B>
class DivisionWithRemainder : public B {
public:

    friend void divmod(const DivisionWithRemainder &a, const DivisionWithRemainder &b,
            typename B::Derived &q, typename B::Derived &r) {
        q = a.derived() / b.derived();
        r = a.derived() % b.derived();
    }

    friend typename B::Derived operator/(const DivisionWithRemainder &a,
            const DivisionWithRemainder &b) {
        typename B::Derived q, r;
        divmod(a, b, q, r);
        return q;
    }

    typename B::Derived &operator/=(const DivisionWithRemainder &a) {
        return B::derived() = B::derived() / a.derived();
    }

    friend typename B::Derived operator%(const DivisionWithRemainder &a,
            const DivisionWithRemainder &b) {
        typename B::Derived q, r;
        divmod(a, b, q, r);
        return r;
    }

    typename B::Derived &operator%=(const DivisionWithRemainder &a) {
        return B::derived() = B::derived() % a.derived();
    }

    friend typename B::Derived gcd(const DivisionWithRemainder &a,
            const DivisionWithRemainder &b) {
        typename B::Derived aa = a, bb = b, q, r;
        while(bb != a.zero()) {
            q = aa / bb;
            r = aa % bb;
            aa = bb;
            bb = r;
        }
        return aa;
    }

    friend typename B::Derived gcd(const DivisionWithRemainder &a,
            const DivisionWithRemainder &b,
            typename B::Derived &u, typename B::Derived &v) {
        typename B::Derived aa = a, bb = b, q, r;
        typename B::Derived u1, v1, u2, v2;
        u =  a.one(); v =  a.zero();
        u1 = a.zero(); v1 = a.one();
        while(bb != a.zero()) {
            q = aa / bb;
            r = aa % bb;
            u2 = u - q*u1;
            v2 = v - q*v1;
            u = u1;
            v = v1;
            u1 = u2;
            v1 = v2;
            aa = bb;
            bb = r;
        }
        return aa;
    }
};



template<typename D>
class Group : public AddInverse<HasZero<AddAssociativity<Addition<Equatable<Base<D>>>>>> { };

template<typename D>
class AbelianGroup : public AddCommutativity<Group<D>> { };

template<typename D>
class Ring : public Distributivity<HasOne<MulAssociativity<Multiplication<AbelianGroup<D>>>>> { };

template<typename D>
class CommutativeRing : public MulCommutativity<Ring<D>> { };

template<typename D>
class IntegralDomain : public NoZeroDivisors<CommutativeRing<D>> { };

template<typename D>
class EuclideanDomain : public DivisionWithRemainder<IntegralDomain<D>> { };

template<typename D>
class Field : public MulInverse<IntegralDomain<D>> { };


/*
template<typename E>
class 
    Z operator-() const { return Z(n - val, true); }

    friend Z operator+(Z a, Z b) { return Z(T(a) + T(b)); }
    friend Z operator-(Z a, Z b) { return Z(a.val < b.val ? n - (b.val - a.val) : a.val - b.val, true); }
    friend Z operator*(Z a, Z b) { return Z(T(a) * T(b)); }

    Z inv() const {
        Z u, v;
        //return ext_euclid(T(val), T(n), u, v) == T(1) ? u : Z();
        if(ext_euclid(T(val), T(n), u, v) != T(1))
            throw std::domain_error("Non-invertible Z<n> element");
        return u;
    }
    friend Z operator/(Z a, Z b) { return a * b.inv(); }

    friend bool operator==(Z a, Z b) { return a.val == b.val; }
    friend bool operator!=(Z a, Z b) { return !(a == b); }
*/
}
}

#endif

