
#ifndef BIGINT_H
#define BIGINT_H

#include <stdexcept>
#include <cstdint>
#include <memory>
#include <algorithm>
#include <ostream>
#include <string>
#include <utility>
#include <random>

#include "debug.h"

namespace crypt {
namespace bigint {

template<typename D, typename DD>
class SinglePrecC {
public:
    typedef D digit;
    typedef unsigned bits_t;
    static const bits_t bits1 = sizeof(digit) * 8;
    static const digit max1 = ~digit(0);
    static const digit lastbit1 = digit(1) << (bits1 - 1);

private:
    typedef DD dbldigit;
    static const dbldigit mask = max1;

    static void split(dbldigit a, digit &lo, digit &hi) noexcept {
        lo = a & mask;
        hi = a >> bits1;
    }

    static dbldigit combine(digit lo, digit hi) noexcept {
        return (dbldigit(hi) << bits1) | lo;
    }

protected:
    static std::string to_string1(digit a, bool pad = true) {
        static const char hex[] = "0123456789abcdef";
        std::string s = "";
        bool zeros = true;
        for(bits_t shift = bits1; shift != 0; shift -= 4) {
            unsigned d = (a >> (shift - 4)) & 0xf;
            if(pad || d != 0 || !zeros) {
                s += hex[d];
                zeros = false;
            }
        }
        return s;
    }

    static void lshift1(digit a, bits_t k, digit &r_lo, digit &r_hi /*inout*/) noexcept {
        digit hi;
        split(dbldigit(a) << k, r_lo, hi);
        r_hi |= hi;
    }

    static void rshift1(digit a, bits_t k, digit &r_lo, digit &r_lolo /*inout*/) noexcept {
        digit lolo;
        split(combine(0, a) >> k, lolo, r_lo);
        r_lolo |= lolo;
    }

    static void add1(digit a, digit b, digit &r, digit &carry /*inout*/) noexcept {
        split(dbldigit(a) + b + carry, r, carry);
    }

    static void sub1(digit a, digit b, digit &r, digit &borrow /*inout*/) noexcept {
        split(dbldigit(a) - b - borrow, r, borrow);
        borrow = -borrow;
    }

    static void muladd1(digit mul1, digit mul2, digit add1, digit add2,
            digit &r_lo, digit &r_hi) noexcept {
        split(dbldigit(mul1) * mul2 + add1 + add2, r_lo, r_hi);
    }

    /* r = a - b*c - d */
    static void submul1(digit a, digit b, digit c, digit d, digit &r, digit &borrow) noexcept {
        split(dbldigit(a) - dbldigit(b) * c - d, r, borrow);
        borrow = -borrow;
    }

    static bits_t normshift(digit a) noexcept {
        bits_t k = 0;
        while((a & lastbit1) == 0) {
            k++;
            a <<= 1;
        }
        return k;
    }

    static void div1(digit a_lo, digit a_hi, digit b, digit &q) noexcept {
        q = combine(a_lo, a_hi) / b;
    }

    static void div1(digit a_lo, digit a_hi, digit b, digit &q, digit &r) noexcept {
        dbldigit a = combine(a_lo, a_hi);
        q = a / b;
        r = a % b;
    }

};


template<typename S>
class Basic : public S {
public:
    typedef typename S::digit digit;
    typedef typename S::bits_t bits_t;
    typedef int dsize_t;

    static void zero(digit *a, dsize_t asz) noexcept {
        for(dsize_t i = 0; i < asz; i++) {
            a[i] = 0;
        }
    }

    static void copy(const digit *a, dsize_t asz, digit *b) noexcept {
        for(dsize_t i = 0; i < asz; i++) {
            b[i] = a[i];
        }
    }

    static bool equals(const digit *a, dsize_t asz, const digit *b) noexcept {
        for(dsize_t i = asz - 1; i >= 0; i--) {
            if(a[i] != b[i]) {
                return false;
            }
        }
        return true;
    }

    static bool less(const digit *a, dsize_t asz, const digit *b) noexcept {
        for(dsize_t i = asz - 1; i >= 0; i--) {
            if(a[i] > b[i]) {
                return false;
            } else if(a[i] < b[i]) {
                return true;
            }
        }
        return false;
    }

    /** shift `a` left by `n*bits1+k` bits, `k < bits1`; return the most significant digit.
     *  `r` is `asz + n` long. 
     *  `a` == `r` is allowed. */
    static digit lshift(const digit *a, dsize_t asz, dsize_t n, bits_t k, digit *r) noexcept {
        digit rd = 0;
        r += n;
        if(k != 0) {
            if(asz > 0) {
                S::lshift1(a[asz - 1], k, r[asz - 1], rd);
            }
            for(dsize_t i = asz - 2; i >= 0; i--) {
                S::lshift1(a[i], k, r[i], r[i + 1]);
            }
        } else if(a != r) {
            for(dsize_t i = asz - 1; i >= 0; i--) {
                r[i] = a[i];
            }
        }
        r -= n;
        zero(r, n);
        return rd;
    }

    /** shift `a` right by `n*bits1+k` bits, `k < bits1`.
     *  `r` is `max(asz - n, 0)` long. 
     *  `a` == `r` is allowed. */
    static void rshift(const digit *a, dsize_t asz, dsize_t n, bits_t k, digit *r) noexcept {
        if(asz > n) {
            a += n;
            asz -= n;
            if(k != 0) {
                digit garb;
                S::rshift1(a[0], k, r[0], garb);
                for(dsize_t i = 1; i < asz; i++) {
                    S::rshift1(a[i], k, r[i], r[i - 1]);
                }
            } else if(a != r) {
                for(dsize_t i = 0; i < asz; i++) {
                    r[i] = a[i];
                }
            }
        }
    }

    /** add `a` to `b`, return carry.
     *  `r` is max(asz, bsz) long.
     *  `a` == `r` or `b` == `r` is allowed. */
    static digit add(const digit *a, dsize_t asz, const digit *b, dsize_t bsz,
            digit *r) noexcept {
        dsize_t minsz = std::min(asz, bsz);
        digit carry = 0;
        dsize_t i = 0;
        for(; i < minsz; i++) {
            S::add1(a[i], b[i], r[i], carry);
        }
        for(; i < asz; i++) {
            S::add1(a[i], 0, r[i], carry);
        }
        for(; i < bsz; i++) {
            S::add1(0, b[i], r[i], carry);
        }
        return carry;
    }

    /** subtract `b` from `a`, return borrow (0 or 1).
     *  `r` is max(asz, bsz) long.
     *  `a` == `r` or `b` == `r` is allowed. */
    static digit sub(const digit *a, dsize_t asz, const digit *b, dsize_t bsz,
            digit *r) noexcept {
        dsize_t minsz = std::min(asz, bsz);
        digit borrow = 0;
        dsize_t i = 0;
        for(; i < minsz; i++) {
            S::sub1(a[i], b[i], r[i], borrow);
        }
        for(; i < asz; i++) {
            S::sub1(a[i], 0, r[i], borrow);
        }
        for(; i < bsz; i++) {
            S::sub1(0, b[i], r[i], borrow);
        }
        return borrow;
    }

    static std::string to_string(const digit *a, dsize_t asz) {
        std::string s = "";
        bool pad = false;
        for(dsize_t i = asz - 1; i >= 0; i--) {
            s += S::to_string1(a[i], pad);
            pad = true;
        }
        return s;
    }
};


//template<typename S>
//class Basic : public S {
//public:
//    typedef typename S::digit digit;
//    typedef typename S::bits_t bits_t;
//    typedef int dsize_t;
//
//    static void zero(digit *a, dsize_t asz) noexcept {
//        digit *const aend = a + asz;
//        for(; a < aend; a++) {
//            *a = 0;
//        }
//    }
//
//    static void copy(const digit *a, dsize_t asz, digit *b) noexcept {
//        const digit *const aend = a + asz;
//        for(; a < aend; a++, b++) {
//            *b = *a;
//        }
//    }
//
//    static bool equals(const digit *a, dsize_t asz, const digit *b) noexcept {
//        const digit *const abeg1 = a - 1;
//        for(a += asz - 1, b += asz - 1; a > abeg1; a--, b--) {
//            if(*a != *b) {
//                return false;
//            }
//        }
//        return true;
//    }
//
//    static bool less(const digit *a, dsize_t asz, const digit *b) noexcept {
//        const digit *const abeg1 = a - 1;
//        for(a += asz - 1, b += asz - 1; a > abeg1; a--, b--) {
//            if(*a > *b) {
//                return false;
//            } else if(*a < *b) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    /** shift `a` left by `n*bits1+k` bits, `k < bits1`; return the most significant digit.
//     *  `r` is `asz + n` long. 
//     *  `a` == `r` is allowed. */
//    static digit lshift(const digit *a, dsize_t asz, dsize_t n, bits_t k, digit *r) noexcept {
//        digit rd = 0;
//        r += n;
//        if(k != 0) {
//            if(asz > 0) {
//                S::lshift1(a[asz - 1], k, r[asz - 1], rd);
//            }
//            for(dsize_t i = asz - 2; i >= 0; i--) {
//                S::lshift1(a[i], k, r[i], r[i + 1]);
//            }
//        } else if(a != r) {
//            for(dsize_t i = asz - 1; i >= 0; i--) {
//                r[i] = a[i];
//            }
//        }
//        r -= n;
//        zero(r, n);
//        return rd;
//    }
//
//    /** shift `a` right by `n*bits1+k` bits, `k < bits1`.
//     *  `r` is `max(asz - n, 0)` long. 
//     *  `a` == `r` is allowed. */
//    static void rshift(const digit *a, dsize_t asz, dsize_t n, bits_t k, digit *r) noexcept {
//        if(asz > n) {
//            a += n;
//            asz -= n;
//            if(k != 0) {
//                digit garb;
//                S::rshift1(a[0], k, r[0], garb);
//                for(dsize_t i = 1; i < asz; i++) {
//                    S::rshift1(a[i], k, r[i], r[i - 1]);
//                }
//            } else if(a != r) {
//                for(dsize_t i = 0; i < asz; i++) {
//                    r[i] = a[i];
//                }
//            }
//        }
//    }
//
//    /** add `a` to `b`, return carry.
//     *  `r` is max(asz, bsz) long.
//     *  `a` == `r` or `b` == `r` is allowed. */
//    static digit add(const digit *a, dsize_t asz, const digit *b, dsize_t bsz,
//            digit *r) noexcept {
//        const digit *const aend = a + asz, *const bend = b + bsz,
//                *const minend = a + std::min(asz, bsz);
//        digit carry = 0;
//        for(; a < minend; a++, b++, r++) {
//            S::add1(*a, *b, *r, carry);
//        }
//        for(; a < aend; a++, r++) {
//            S::add1(*a, 0, *r, carry);
//        }
//        for(; b < bend; b++, r++) {
//            S::add1(0, *b, *r, carry);
//        }
//        return carry;
//    }
//
//    /** subtract `b` from `a`, return borrow (0 or 1).
//     *  `r` is max(asz, bsz) long.
//     *  `a` == `r` or `b` == `r` is allowed. */
//    static digit sub(const digit *a, dsize_t asz, const digit *b, dsize_t bsz,
//            digit *r) noexcept {
//        dsize_t minsz = std::min(asz, bsz);
//        digit borrow = 0;
//        dsize_t i = 0;
//        for(; i < minsz; i++) {
//            S::sub1(a[i], b[i], r[i], borrow);
//        }
//        for(; i < asz; i++) {
//            S::sub1(a[i], 0, r[i], borrow);
//        }
//        for(; i < bsz; i++) {
//            S::sub1(0, b[i], r[i], borrow);
//        }
//        return borrow;
//    }
//
//    static std::string to_string(const digit *a, dsize_t asz) {
//        std::string s = "";
//        bool pad = false;
//        for(dsize_t i = asz - 1; i >= 0; i--) {
//            s += S::to_string1(a[i], pad);
//            pad = true;
//        }
//        return s;
//    }
//};


template<typename S>
class SchoolMul : public S {
public:
    typedef typename S::digit digit;
    typedef typename S::dsize_t dsize_t;

    static constexpr dsize_t mul_tmpsz(dsize_t /*asz*/, dsize_t /*bsz*/) noexcept {
        return 0;
    }

    /** multiply `a` by `b`.
     *  `r` must zeroed and `asz` + `bsz` long. */
    static void mul(const digit *a, dsize_t asz, const digit *b, dsize_t bsz,
            digit *r, digit* /*tmp*/) noexcept {
        for(dsize_t i = 0; i < bsz; i++) {
            if(b[i] != 0) {
                digit carry = 0;
                for(dsize_t j = 0; j < asz; j++) {
                    S::muladd1(a[j], b[i], r[i+j], carry, r[i+j], carry);
                }
                r[i + asz] = carry;
            }
        }
    }
};


//template<typename S>
//class SchoolMul : public S {
//public:
//    typedef typename S::digit digit;
//    typedef typename S::dsize_t dsize_t;
//
//    static constexpr dsize_t mul_tmpsz(dsize_t /*asz*/, dsize_t /*bsz*/) noexcept {
//        return 0;
//    }
//
//    /** multiply `a` by `b`.
//     *  `r` must zeroed and `asz` + `bsz` long. */
//    static void mul(const digit *a, dsize_t asz, const digit *b, dsize_t bsz,
//            digit *r, digit* /*tmp*/) noexcept {
//        const digit *const aend = a + asz, *const abeg = a, *const bend = b + bsz;
//        for(; b < bend; b++, r++) {
//            if(*b != 0) {
//                digit carry = 0;
//                digit *rp = r;
//                for(a = abeg; a < aend; a++, rp++) {
//                    S::muladd1(*a, *b, *rp, carry, *rp, carry);
//                }
//                *rp = carry;
//            }
//        }
//    }
//};


template<typename S>
class SchoolDiv : public S {
public:
    typedef typename S::digit digit;
    typedef typename S::dsize_t dsize_t;

protected:
    static void div_nby1(const digit *a, dsize_t asz, digit b, digit *q, digit &r) noexcept {
        r = 0;
        for(dsize_t i = asz - 1; i >= 0; i--) {
            S::div1(a[i], r, b, q[i], r);
        }
    }

    static digit submul(digit *a, const digit *b, dsize_t bsz, digit q) noexcept {
        digit borrow = 0;
        for(dsize_t i = 0; i < bsz; i++) {
            S::submul1(a[i], b[i], q, borrow, a[i], borrow);
        }
        S::sub1(a[bsz], digit(0), a[bsz], borrow);
        return borrow;
    }

    // overflow ignored
    static digit div_nbyn1(digit *a, const digit *b, dsize_t bsz) noexcept {
        digit q = 0;
        if(a[bsz] >= b[bsz - 1]) {
            q = S::max1;
        } else {
            S::div1(a[bsz - 1], a[bsz], b[bsz - 1], q);
        }
        digit borrow = submul(a, b, bsz, q);
        while(borrow != 0) {
            q--;
            borrow -= S::add(a, bsz + 1, b, bsz, a);
        }
        return q;
    }

public:
    static constexpr dsize_t div_tmpsz(dsize_t asz, dsize_t bsz) {
        return (bsz == 1 || asz <= bsz) ? 0 : asz + bsz + 1;
    }

    /** `a[asz - 1] != 0 && b[bsz - 1] != 0`
     *  `q` is `max(asz - bsz + 1, 1)` long
     *  `r` is `bsz` long */
    static void div(const digit *a, dsize_t asz, const digit *b, dsize_t bsz,
            digit *q, digit *r, digit *tmp) noexcept {
        if(bsz == 1) {
            div_nby1(a, asz, b[0], q, r[0]);
        } else if(asz < bsz) {
            q[0] = 0;
            S::copy(a, asz, r);
            S::zero(r + asz, bsz - asz);
        } else if(asz == bsz) {
            if(S::less(a, asz, b)) {
                q[0] = 0;
                S::copy(a, asz, r);
            } else {
                q[0] = 1;
                S::sub(a, asz, b, bsz, r);
            }
        } else {
            digit *aa = tmp, *bb = tmp + asz + 1;
            typename S::bits_t k = S::normshift(b[bsz - 1]);
            aa[asz] = S::lshift(a, asz, 0, k, aa);
            S::lshift(b, bsz, 0, k, bb);

            for(dsize_t i = asz - bsz; i >= 0; i--) {
                q[i] = div_nbyn1(aa + i, bb, bsz); // overflow is never possible
            }

            S::rshift(aa, bsz, 0, k, r);
        }
    }
};


template<typename S>
class UIntegerT {
public:
    typedef typename S::bits_t bits_t;
protected:
    typedef typename S::digit digit;
    typedef typename S::dsize_t dsize_t;

    struct Tmp {
        std::unique_ptr<digit[]> ptr;
        Tmp(dsize_t sz) : ptr(sz ? new digit[sz] : nullptr) { }
        operator digit *() noexcept { return ptr.get(); }
    };

    dsize_t size, alloc_size;
    std::unique_ptr<digit[]> digits;

    void set_size(dsize_t new_size, bool copy = true) {
        dsize_t new_alloc_size = alloc_size ? alloc_size : 4;
        while(new_size > new_alloc_size) new_alloc_size <<= 1; 
        if(new_alloc_size > alloc_size) {
            decltype(digits) new_digits(new digit[new_alloc_size]);
            if(copy) {
                S::copy(digits.get(), size, new_digits.get());
            }
            digits = std::move(new_digits);
            alloc_size = new_alloc_size;
        }
        if(copy) {
            S::zero(digits.get() + size, new_size - size); 
        }
        size = new_size;
    }

    void normalize() noexcept {
        while(size > 0 && digits[size - 1] == 0) {
            size--;
        }
    }

public:

    UIntegerT() : size(0), alloc_size(0) {}

    UIntegerT(std::string s) : UIntegerT() {
        for(auto i = s.begin(); i != s.end(); i++) {
            int dval = -1;
            if('0' <= *i && *i <= '9') {
                dval = *i - '0';
            } else if('a' <= *i && *i <= 'z') {
                dval = *i - 'a' + 10;
            } else if('A' <= *i && *i <= 'Z') {
                dval = *i - 'A' + 10;
            }
            if(dval >= 0) {
                *this <<= 4;
                if(size == 0) set_size(1);
                digits[0] |= dval;
            }
        }
        normalize();
    }

    template<typename R>
    UIntegerT(bits_t bits, R &random_generator) : UIntegerT() {
        dsize_t ndig = bits / S::bits1;
        bits_t nbit = bits % S::bits1;
        set_size(ndig + (nbit == 0 ? 0 : 1));
        std::uniform_int_distribution<digit> u(0, S::max1);
        for(int i = 0; i < size; i++) {
            digits[i] = u(random_generator);
        }
        digits[size - 1] |= S::lastbit1;
        if(nbit != 0) {
            digits[size - 1] >>= S::bits1 - nbit;
        }
    }

    UIntegerT(const UIntegerT &a) : UIntegerT() {
        set_size(a.size, false);
        S::copy(a.digits.get(), a.size, digits.get());
    }

    UIntegerT(UIntegerT &&a) noexcept : size(a.size), alloc_size(a.alloc_size),
            digits(std::move(a.digits)) {}

    ~UIntegerT() {}

    UIntegerT &operator=(const UIntegerT &a) {
        set_size(a.size, false);
        S::copy(a.digits.get(), a.size, digits.get());
        return *this;
    }

    UIntegerT &operator=(UIntegerT &&a) noexcept {
        size = a.size;
        alloc_size = a.alloc_size;
        digits = std::move(a.digits);
    }

    std::string to_string() const {
        return S::to_string(digits.get(), size);
    }

    friend bool operator==(const UIntegerT &a, const UIntegerT &b) noexcept {
        return a.size == b.size && S::equals(a.digits.get(), a.size, b.digits.get());
    }

    friend bool operator<(const UIntegerT &a, const UIntegerT &b) noexcept {
        return a.size < b.size || (a.size == b.size && S::less(a.digits.get(), a.size, b.digits.get()));
    }

    friend std::ostream &operator<<(std::ostream &os, const UIntegerT &a) {
        os << a.to_string();
        return os;
    }

    UIntegerT &operator<<=(bits_t k) {
        dsize_t shdig = k / S::bits1;
        bits_t shbit = k % S::bits1;
        dsize_t old_size = size;
        set_size(size + shdig + (shbit == 0 ? 0 : 1));
        digit d = S::lshift(digits.get(), old_size, shdig, shbit, digits.get());
        if(shbit != 0) {
            digits[size - 1] = d;
        }
        normalize();
        return *this;
    }

    friend UIntegerT operator<<(const UIntegerT &a, bits_t k) {
        return UIntegerT(a) <<= k;
    }

    UIntegerT &operator>>=(bits_t k) {
        dsize_t shdig = k / S::bits1;
        bits_t shbit = k % S::bits1;
        S::rshift(digits.get(), size, shdig, shbit, digits.get());
        set_size(std::max(size - shdig, 0));
        normalize();
        return *this;
    }

    friend UIntegerT operator>>(const UIntegerT &a, bits_t k) {
        return UIntegerT(a) >>= k;
    }

    UIntegerT &operator+=(const UIntegerT &a) {
        dsize_t old_size = size;
        set_size(std::max(size, a.size) + 1);
        digits[size - 1] = S::add(digits.get(), old_size, a.digits.get(), a.size, digits.get());
        normalize();
        return *this;
    }

    friend UIntegerT operator+(const UIntegerT &a, const UIntegerT &b) {
        return UIntegerT(a) += b;
    }

    UIntegerT &operator-=(const UIntegerT &a) {
        dsize_t old_size = size;
        set_size(std::max(size, a.size));
        digit overflow = S::sub(digits.get(), old_size, a.digits.get(), a.size, digits.get());
        normalize();
        if(overflow != 0) {
            throw std::overflow_error("UInteger result <0");
        }
        return *this;
    }

    friend UIntegerT operator-(const UIntegerT &a, const UIntegerT &b) {
        return UIntegerT(a) -= b;
    }

    friend UIntegerT operator*(const UIntegerT &a, const UIntegerT &b) {
        UIntegerT r;
        r.set_size(a.size + b.size);
        auto tmp = Tmp(S::mul_tmpsz(a.size, b.size));
        S::mul(a.digits.get(), a.size, b.digits.get(), b.size, r.digits.get(), tmp);
        r.normalize();
        return r;
    }

    UIntegerT &operator*=(const UIntegerT &a) {
        return *this = *this * a;
    }

    friend void divmod(const UIntegerT &a, const UIntegerT &b, UIntegerT &q, UIntegerT &r) {
        if(b.size == 0) {
            throw std::domain_error("UInteger division by 0");
        }
        q.set_size(std::max(a.size - b.size + 1, 1));
        r.set_size(b.size);
        auto tmp = Tmp(S::div_tmpsz(a.size, b.size));
        S::div(a.digits.get(), a.size, b.digits.get(), b.size,
                q.digits.get(), r.digits.get(), tmp);
        q.normalize();
        r.normalize();
    }

    friend UIntegerT operator/(const UIntegerT &a, const UIntegerT &b) {
        UIntegerT q, r;
        divmod(a, b, q, r);
        return q;
    }

    friend UIntegerT operator%(const UIntegerT &a, const UIntegerT &b) {
        UIntegerT q, r;
        divmod(a, b, q, r);
        return r;
    }

};

template<typename D, typename DD>
using UIntegerS = UIntegerT<SchoolDiv<SchoolMul<Basic<SinglePrecC<D, DD>>>>>;

typedef UIntegerS<uint8_t, uint16_t> UInteger8;
typedef UIntegerS<uint16_t, uint32_t> UInteger16;
typedef UIntegerS<uint32_t, uint64_t> UInteger32;

#ifdef _GLIBCXX_USE_INT128
typedef UIntegerS<uint64_t, unsigned __int128> UInteger64;
typedef UInteger64 UInteger;
#else
typedef UInteger32 UInteger;
#endif


using namespace std::rel_ops;

}
}

#endif

