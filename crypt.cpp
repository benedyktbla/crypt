
#include "crypt.h"

namespace crypt {

void Padding::Pad::set(unsigned block_size, std::istream &is) {
    auto pos = is.tellg();
    stream_size = is.seekg(0, std::ios::end).tellg() - pos;
    is.seekg(pos);
    InBlockIterator::set(block_size, is);
}

Cipher::Cipher(unsigned block_size) : block_size(block_size) { }

Cipher::~Cipher() { }

unsigned Cipher::get_block_size() {
    return block_size;
}

void Cipher::encrypt(std::istream &in, std::ostream &out, Padding::Pad &pad) {
    //in.exceptions(std::ios::failbit | std::ios::badbit);//TODO
    //out.exceptions(std::ios::failbit | std::ios::badbit);

    pad.set(block_size, in);
    util::OutBlockIterator ob;
    ob.set(block_size, out);

    while(++pad) {
        encrypt_block(reinterpret_cast<const unsigned char *>(*pad),
                reinterpret_cast<unsigned char *>(*ob));
        ++ob;
    }
}

void Cipher::decrypt(std::istream &in, std::ostream &out, Padding::UnPad &unpad) {
    //in.exceptions(std::ios::failbit | std::ios::badbit);//TODO
    //out.exceptions(std::ios::failbit | std::ios::badbit);

    util::InBlockIterator ib;
    ib.set(block_size, in);
    unpad.set(block_size, out);

    while(++ib) {
        decrypt_block(reinterpret_cast<const unsigned char *>(*ib),
                reinterpret_cast<unsigned char *>(*unpad));
        ++unpad;
    }
}

}

