
#ifndef PADDING_H
#define PADDING_H

#include "crypt.h"

namespace crypt {

class TwoPadding : public Padding {
public:
    class Pad : public Padding::Pad {
    public:
        void set(unsigned block_size, std::istream &is) override;
        Pad &operator++() override; //prefix
    protected:
        bool first;
    };

    class UnPad : public Padding::UnPad {
    public:
        void set(unsigned block_size, std::ostream &os) override;
        UnPad &operator++() override; //prefix
    protected:
        bool first;
    };
};

}

#endif

