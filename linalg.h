
#ifndef LINALG_H
#define LINALG_H

#include <initializer_list>
#include <type_traits>
#include <stdexcept>
#include <memory>
#include <string>
#include <ostream>

#include "util.h"

namespace crypt {
namespace linalg {

class IMatrix { };
class IFactory { };

template<typename T>
struct is_matrix : public util::yesno_enable<std::is_base_of<IMatrix, T>::value> { };

template<typename T>
struct is_factory : public util::yesno_enable<std::is_base_of<IFactory, T>::value> { };

template<typename Ma, typename Mb>
void copy(Ma &dst, const Mb &src) {
    typedef decltype(const_cast<const Ma &>(dst)(0, 0)) T;
    for(unsigned i = 0; i < dst.rows(); i++) {
        for(unsigned j = 0; j < dst.cols(); j++) {
            dst(i, j) = T(src(i, j));
        }
    }
}

template<typename T>
class Array1Matrix : public IMatrix {
    unsigned rows_, cols_;
    T *array;
public:
    Array1Matrix(unsigned rows, unsigned cols, T *array)
            : rows_(rows), cols_(cols), array(array) { }
    unsigned rows() const { return rows_; }
    unsigned cols() const { return cols_; }
    T operator()(unsigned i, unsigned j) const { return array[i*cols() + j]; }
    T &operator()(unsigned i, unsigned j) { return array[i*cols() + j]; }
};

template<bool evaluated_ = false>
class Expression : public IMatrix {
    unsigned rows_, cols_;
public:
    static bool evaluated() { return evaluated_; }
    Expression(unsigned rows, unsigned cols) : rows_(rows), cols_(cols) { }
    unsigned rows() const { return rows_; }
    unsigned cols() const { return cols_; }
};

template<typename M>
class Slice : public Expression<> {
    const M &a;
    unsigned row_offset, col_offset;
public:
    Slice(const M &a, unsigned row_offset, unsigned col_offset, unsigned rows, unsigned cols)
            : Expression(rows, cols), a(a), row_offset(row_offset), col_offset(col_offset) { }
    auto operator()(unsigned i, unsigned j) const { return a(row_offset + i, col_offset + j); }
};

template<typename M, bool = is_matrix<M>::yes>
auto slice(const M &a, unsigned row_offset, unsigned col_offset, unsigned rows, unsigned cols) {
        return Slice<M>(a, row_offset, col_offset, rows, cols);
}

template<typename M, bool = is_matrix<M>::yes>
auto slice(const M &a, unsigned rows, unsigned cols) {
        return slice(a, 0, 0, rows, cols);
}

template<typename T>
class Fill : public Expression<true> {
    T q;
public:
    Fill(T q, unsigned rows, unsigned cols) : Expression(rows, cols), q(q) { }
    auto operator()(unsigned, unsigned) const { return q; }
};

template<typename T>
class FillFactory : public IFactory {
    T q;
public:
    FillFactory(T q) : q(q) { }

    template<typename U>
    auto get(util::marker<U> &&, unsigned rows, unsigned cols) const { return Fill<U>(U(q), rows, cols); }
};

template<typename T>
auto fill(T q) { return FillFactory<T>(q); }

template<typename T>
auto fill(T q, unsigned rows, unsigned cols) { return Fill<T>(q, rows, cols); }

template<typename T>
class Id : public Expression<true> {
public:
    Id(unsigned rows) : Expression(rows, rows) { }
    auto operator()(unsigned i, unsigned j) const { return (i == j) ? T(1) : T(0); }
};

class IdFactory : public IFactory {
public:
    template<typename T>
    auto get(util::marker<T> &&, unsigned rows, unsigned) const { return Id<T>(rows); }
};

inline auto id() { return IdFactory(); }

template<typename T>
auto id(unsigned rows) { return Id<T>(rows); }

template<typename T, unsigned m, unsigned n>
class SMatrix : public IMatrix {
    T elements[m][n];
public:
    static constexpr bool evaluated() { return true; }
    static constexpr unsigned rows() { return m; }
    static constexpr unsigned cols() { return n; }

    SMatrix() : SMatrix(fill(T(0))) { }

    SMatrix(const SMatrix &a) { copy(*this, a); }

    template<typename M, bool = is_matrix<M>::yes>
    SMatrix(const M &a) {
        if(a.rows() != rows() || a.cols() != cols()) {
            throw std::invalid_argument("Can't initialize with matrix of these dimensions");
        }
        copy(*this, a);
    }

    template<typename F, bool = is_factory<F>::yes, int = 0>
    SMatrix(const F &f) : SMatrix(f.get(util::marker<T>(), rows(), cols())) { }

    template<typename U>
    SMatrix(U *array) : SMatrix(Array1Matrix<U>(rows(), cols(), array)) { }

    template<typename U>
    SMatrix(std::initializer_list<U> il) : SMatrix(il.begin()) {
        //TODO static_assert(il.size() == m*n, "Wrong size of initializer list");
    }

    SMatrix &operator=(const SMatrix &a) { copy(*this, a); return *this; }

    template<typename M, bool = is_matrix<M>::yes>
    SMatrix &operator=(const M &a) {
        if(a.rows() != rows() || a.cols() != cols()) {
            throw std::invalid_argument("Can't assign a matrix of these dimensions");
        }
        if(a.evaluated()) {
            copy(*this, a);
        } else {
            *this = SMatrix(a);
        }
        return *this;
    }

    T operator()(unsigned i, unsigned j) const { return elements[i][j]; }
    T &operator()(unsigned i, unsigned j) { return elements[i][j]; }

    template<typename U> SMatrix &operator+=(const U &a) { return *this = *this + a; }
    template<typename U> SMatrix &operator-=(const U &a) { return *this = *this - a; }
    template<typename U> SMatrix &operator*=(const U &a) { return *this = *this * a; }

    // 1x1

    template<typename U, typename M = SMatrix,
            bool = util::yesno_enable<M::rows() == 1 && M::cols() == 1>::yes,
            bool = is_matrix<U>::no, bool = is_factory<U>::no>
    SMatrix(U q) { (*this)(0, 0) = q; }

    template<typename U, typename M = SMatrix,
            bool = util::yesno_enable<M::rows() == 1 && M::cols() == 1>::yes,
            bool = is_matrix<U>::no, bool = is_factory<U>::no>
    SMatrix &operator=(U q) { (*this)(0, 0) = q; return *this; }

    template<typename M = SMatrix,
            bool = util::yesno_enable<M::rows() == 1 && M::cols() == 1>::yes>
    explicit operator T() const { return (*this)(0, 0); }
};

template<typename T>
class DMatrix : public IMatrix {
    unsigned rows_, cols_;
    std::unique_ptr<T[]> elements;
public:
    static bool evaluated() { return true; }

    DMatrix() = delete;

    DMatrix(const DMatrix &a) : rows_(a.rows()), cols_(a.cols()),
            elements(new T[a.rows() * a.cols()]) { copy(*this, a); }

    DMatrix(DMatrix &&a) noexcept : rows_(a.rows()), cols_(a.cols()), elements(std::move(a.elements)) { }

    template<typename M, bool = is_matrix<M>::yes>
    DMatrix(const M &a) : rows_(a.rows()), cols_(a.cols()),
            elements(new T[a.rows() * a.cols()]) { copy(*this, a); }

    DMatrix(unsigned rows, unsigned cols) : DMatrix(rows, cols, fill(T(0))) { }

    template<typename F, bool = is_factory<F>::yes>
    DMatrix(unsigned rows, unsigned cols, const F &f)
            : DMatrix(f.get(util::marker<T>(), rows, cols)) { }

    template<typename U>
    DMatrix(unsigned rows, unsigned cols, U *array)
            : DMatrix(Array1Matrix<U>(rows, cols, array)) { }

    template<typename U>
    DMatrix(unsigned rows, unsigned cols, std::initializer_list<U> il)
            : DMatrix(rows, cols, il.begin()) {
        if(il.size() != rows_ * cols_) { /* TODO */ }
    }

    DMatrix &operator=(const DMatrix &a) {
        if(a.rows() != rows() || a.cols() != cols()) {
            throw std::invalid_argument("Can't assign a matrix of these dimensions");
        }
        copy(*this, a);
        return *this;
    }

    DMatrix &operator=(DMatrix &&a) noexcept {
        if(a.rows() != rows() || a.cols() != cols()) {
            throw std::invalid_argument("Can't assign a matrix of these dimensions");
        }
        elements = std::move(a.elements);
        return *this;
    }

    template<typename M, bool = is_matrix<M>::yes>
    DMatrix &operator=(const M &a) {
        if(a.rows() != rows() || a.cols() != cols()) {
            throw std::invalid_argument("Can't assign a matrix of these dimensions");
        }
        if(a.evaluated()) {
            copy(*this, a);
        } else {
            *this = DMatrix(a);
        }
        return *this;
    }

    template<typename F, bool = is_factory<F>::yes, int = 0>
    DMatrix &operator=(const F &f) {
        return *this = f.get(util::marker<T>(), rows(), cols());
    }

    ~DMatrix() { }

    unsigned rows() const { return rows_; }
    unsigned cols() const { return cols_; }

    T operator()(unsigned i, unsigned j) const { return elements[i*cols() + j]; }
    T &operator()(unsigned i, unsigned j) { return elements[i*cols() + j]; }

    template<typename U> DMatrix &operator+=(const U &a) { return *this = *this + a; }
    template<typename U> DMatrix &operator-=(const U &a) { return *this = *this - a; }
    template<typename U> DMatrix &operator*=(const U &a) { return *this = *this * a; }
};

template<typename Ma, typename Mb,
    bool = is_matrix<Ma>::yes, bool = is_matrix<Mb>::yes>
bool operator==(const Ma &a, const Mb &b) {
    if(a.rows() != b.rows() || a.cols() != b.cols()) {
        return false;
    }
    for(unsigned i = 0; i < a.rows(); i++) {
        for(unsigned j = 0; j < a.cols(); j++) {
            if(a(i, j) != b(i, j)) {
                return false;
            }
        }
    }
    return true;
}

template<typename M, typename F, int = 0,
    bool = is_matrix<M>::yes, bool = is_factory<F>::yes>
bool operator==(const M &a, const F &b) {
    return a == b.get(util::marker<decltype(a(0, 0))>(), a.rows(), a.cols());
}

template<typename Ma, typename Mb,
    bool = is_matrix<Ma>::yes>
bool operator!=(const Ma &a, const Mb &b) {
    return !(a == b);
}

template<typename Ma, typename Mb>
class Sum : public Expression<> {
    const Ma &a; const Mb &b;
public:
    Sum(const Ma &a, const Mb &b) : Expression(a.rows(), a.cols()), a(a), b(b) {
        if(a.rows() != b.rows() || a.cols() != b.cols()) {
            throw std::domain_error("Matrices dimensions incompatible with operator+");
        }
    }
    auto operator()(unsigned i, unsigned j) const { return a(i, j) + b(i, j); }
};

template<typename Ma, typename Mb, bool = is_matrix<Ma>::yes, bool = is_matrix<Mb>::yes>
auto operator+(const Ma &a, const Mb &b) { return Sum<Ma, Mb>(a, b); }

template<typename Ma, typename Mb>
class Difference : public Expression<> {
    const Ma &a; const Mb &b;
public:
    Difference(const Ma &a, const Mb &b) : Expression(a.rows(), a.cols()), a(a), b(b) {
        if(a.rows() != b.rows() || a.cols() != b.cols()) {
            throw std::domain_error("Matrices dimensions incompatible with operator-");
        }
    }
    auto operator()(unsigned i, unsigned j) const { return a(i, j) - b(i, j); }
};

template<typename Ma, typename Mb, bool = is_matrix<Ma>::yes, bool = is_matrix<Mb>::yes>
auto operator-(const Ma &a, const Mb &b) { return Difference<Ma, Mb>(a, b); }

template<typename M, typename T>
class SProduct : public Expression<> {
    const M &a;
    T q;
public:
    SProduct(const M &a, T q) : Expression(a.rows(), a.cols()), a(a), q(q) { }
    auto operator()(unsigned i, unsigned j) const { return a(i, j) * q; }
};

template<typename M, typename T, bool = is_matrix<M>::yes, bool = is_matrix<T>::no>
auto operator*(const M &a, T q) { return SProduct<M, T>(a, q); }

template<typename M, typename T, bool = is_matrix<M>::yes, bool = is_matrix<T>::no>
auto operator*(T q, const M &a) { return SProduct<M, T>(a, q); }

template<typename Ma, typename Mb>
class Product : public Expression<> {
    const Ma &a; const Mb &b;
public:
    Product(const Ma &a, const Mb &b) : Expression(a.rows(), b.cols()), a(a), b(b) {
        if(a.cols() != b.rows()) {
            throw std::domain_error("Matrices dimensions incompatible with operator*");
        }
    }
    auto operator()(unsigned i, unsigned j) const {
        decltype(a(0, 0)) s(0);
        for(unsigned k = 0; k < a.cols(); k++) {
            s += a(i, k) * b(k, j);
        }
        return s;
    }
};

template<typename Ma, typename Mb, bool = is_matrix<Ma>::yes, bool = is_matrix<Mb>::yes>
auto operator*(const Ma &a, const Mb &b) { return Product<Ma, Mb>(a, b); }

template<typename M>
class Negation : public Expression<> {
    const M &a;
public:
    Negation(const M &a) : Expression(a.rows(), a.cols()), a(a) { }
    auto operator()(unsigned i, unsigned j) const { return -a(i, j); }
};

template<typename M, bool = is_matrix<M>::yes>
auto operator-(const M &a) { return Negation<M>(a); }

template<typename M>
class Transpose : public Expression<> {
    const M &a;
public:
    Transpose(const M &a) : Expression(a.cols(), a.rows()), a(a) { }
    auto operator()(unsigned i, unsigned j) const { return a(j, i); }
};

template<typename M>
auto transp(const M &a) { return Transpose<M>(a); }

template<typename M>
void swap_rows(M &a, unsigned i, unsigned j) {
    if(i != j) {
        for(unsigned k = 0; k < a.cols(); k++) {
            std::swap(a(i, k), a(j, k));
        }
    }
}

template<typename M, typename T>
void add_rows(M &a, unsigned i, unsigned j, T q) {
    for(unsigned k = 0; k < a.cols(); k++) {
        a(i, k) += q * a(j, k);
    }
}

template<typename M, typename T>
void multiply_row(M &a, unsigned i, T q) {
    for(unsigned j = 0; j < a.cols(); j++) {
        a(i, j) *= q;
    }
}

template<typename Ma, typename Mb>
void gaussian_elimination(Ma &a, Mb &b) {
    if(a.rows() != b.rows()) {
        throw std::domain_error("Can't perform Gaussian elimination on matrices with such dimensions");
    }

    for(unsigned r = 0, c = 0; c < a.cols(); c++) {
        unsigned i = r;
        for(; i < a.rows(); i++) {
            if(is_invertible(a(i, c))) {
                swap_rows(a, r, i);
                swap_rows(b, r, i);
                break;
            }
        }
        if(i >= a.rows()) {
            continue;
        }
        auto q = inv(a(r, c));
        multiply_row(a, r, q);
        multiply_row(b, r, q);
        for(unsigned i = 0; i < a.rows(); i++) {
            if(i != r) {
                auto q = -a(i, c);
                add_rows(a, i, r, q);
                add_rows(b, i, r, q);
            }
        }
        r++;
    }
}

template<typename M>
bool is_reduced_row_echelon(const M &a) {
    unsigned step = 0;
    typename M::element_type zero(0), one(1);
    for(unsigned i = 0; i < a.rows(); i++) {
        unsigned j = 0;
        for(; j < a.cols(); j++) {
            if(a(i, j) != zero) {
                break;
            }
        }
        if(j < a.cols()) {
            if(a(i, j) != one || j < step) {
                return false;
            }
            j++;
            step = j;
            for(; j < a.cols(); j++) {
                if(a(i, j) != one) {
                    return false;
                }
            }
        }
    }
    return true;
}

template<typename M, typename Mr = DMatrix<decltype(std::declval<const M &>()(0, 0))>>
Mr inv(const M &a) {
    if(a.rows() != a.cols()) {
        throw std::domain_error("Can't invert a non-square matrix");
    }
    /*if(a.rows() == 2) {
        try {
            auto d = inv(a(0, 0)*a(1, 1) - a(0, 1)*a(1, 0));
            Mr r(a);
            r(0, 0) = a(1, 1);
            r(0, 1) = -a(0, 1);
            r(1, 0) = -a(1, 0);
            r(1, 1) = a(0, 0);
            r *= d;
            return r;
        } catch(std::domain_error &) {
            throw std::domain_error("Non-invertible matrix");
        }
    } else {*/
        Mr b(a), c(id<decltype(a(0, 0))>(a.rows()));
        gaussian_elimination(b, c);
        if(b != id()) {
            throw std::domain_error("Non-invertible matrix");
        }
        return c;
    //}
}

template<typename M, typename U>
void dump(const M &a, U *array) {
    Array1Matrix<U> wrapper(a.rows(), a.cols(), array);
    copy(wrapper, a);
}

template<typename M, bool = is_matrix<M>::yes>
std::string to_string(const M &a) {
    std::string s;
    for(unsigned i = 0; i < a.rows(); i++) {
        s += "[ ";
        for(unsigned j = 0; j < a.cols(); j++) {
            s += util::to_string(a(i, j)) + " ";
        }
        s += "]\n";
    }
    return s;
}

template<typename M, bool = is_matrix<M>::yes>
std::ostream &operator<<(std::ostream &out, const M &a) {
    return out << to_string(a);
}

template<typename T, unsigned m, unsigned n>
using Matrix = SMatrix<T, m, n>;

}
}

#endif

