
#ifndef ZN_H
#define ZN_H

#include <cstdint>
#include <stdexcept>
#include <string>
#include <ostream>

namespace crypt {
namespace zn {

template<typename T, typename T2>
T ext_euclid(T a, T b, T2 &u, T2 &v) {
    T q, r;
    T2 u1, v1, u2, v2;
    u =  1; v =  0;
    u1 = 0; v1 = 1;
    while(b != T(0)) {
        q = a / b;
        r = a % b;
        u2 = u - q*u1;
        v2 = v - q*v1;
        u = u1;
        v = v1;
        u1 = u2;
        v1 = v2;
        a = b;
        b = r;
    }
    return a;
}

template<bool gt8, bool gt16>
struct Ztypes {
    typedef uint_fast64_t T;
    typedef uint_least32_t Tstore;
};

template<>
struct Ztypes<true, false> {
    typedef uint_fast32_t T;
    typedef uint_least16_t Tstore;
};

template<>
struct Ztypes<false, false> {
    typedef uint_fast16_t T;
    typedef uint_least8_t Tstore;
};

template<uint32_t n, typename T = typename Ztypes<(n > 256), (n > 65536)>::T,
        typename Tstore = typename Ztypes<(n > 256), (n > 65536)>::Tstore>
class Z {
    Tstore val;
    Z(Tstore v, bool) : val(v) { }
public:
    Z() : val(0) { }
    template<typename U> Z(U v) { // TODO FIXME Z((signed char)-128)
        if(v < 0) { val = n  - (-v % n); }
        else { val = v % n; }
    }
    //Z(U v) { val = v % n
    /*Z(int v) { v %= n; val = v >= 0 ? v : v + n; }
    Z(long v) { v %= n; val = v >= 0 ? v : v + n; }
    Z(char v) { T vv = v % n; val = vv >= 0 ? vv : vv + n; }*/
    Z(const Z &a) : val(a.val) { }
    Z operator=(Z a) { val = a.val; return *this; }
    explicit operator T() const { return val; }
    explicit operator Tstore() const { return val; }
    explicit operator int() const { return val; }
    explicit operator long() const { return val; }
    template<typename U> explicit operator U() const { return U(T(*this)); }

    Z operator-() const { return Z(n - val, true); }

    friend Z operator+(Z a, Z b) { return Z(T(a) + T(b)); }
    friend Z operator-(Z a, Z b) { return Z(a.val < b.val ? n - (b.val - a.val) : a.val - b.val, true); }
    friend Z operator*(Z a, Z b) { return Z(T(a) * T(b)); }

    Z inv() const {
        Z u, v;
        //return ext_euclid(T(val), T(n), u, v) == T(1) ? u : Z();
        if(ext_euclid(T(val), T(n), u, v) != T(1))
            throw std::domain_error("Non-invertible Z<n> element");
        return u;
    }
    friend Z operator/(Z a, Z b) { return a * b.inv(); }

    friend bool operator==(Z a, Z b) { return a.val == b.val; }
    friend bool operator!=(Z a, Z b) { return !(a == b); }

    Z operator+=(Z a) { return *this = *this + a; }
    Z operator-=(Z a) { return *this = *this - a; }
    Z operator*=(Z a) { return *this = *this * a; }
    Z operator/=(Z a) { return *this = *this / a; }

    operator std::string() { return std::to_string(long(*this)); }
    friend std::ostream &operator<<(std::ostream &out, Z a) {
        return out << std::string(a);
    }
};

inline bool is_invertible(Z<256> a) {
    return long(a) % 2 == 1;
}

template<uint32_t n>
Z<n> inv(Z<n> a) {
    return a.inv();
}

}
}

#endif
