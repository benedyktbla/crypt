
#ifndef DEBUG_H
#define DEBUG_H

#ifndef NDEBUG

#include <cassert>
#include <iostream>

template<typename T>
inline void dbg(T t) {
    std::cerr << "DBG: " << t << std::endl;
}

template<typename T>
inline void dbg(std::string s, T t) {
    std::cerr << "DBG: " << s << t << std::endl;
}

#else

template<typename T>
inline void dbg(T t) { }

template<typename T>
inline void dbg(std::string s, T t) { }

#endif
#endif

