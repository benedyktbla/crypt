
#include "analysis.h"

namespace crypt {

std::map<const std::string, const std::string> signatures {
    {"PNG", "\x89PNG\r\n\x1A\n"},
    {"ZIP", "PK\x03\x04"},
    {"MP3", "ID3\x03"},
    {"JPG", "\xFF\xD8\xFF\xE0"},
    {"PDF", "%PDF"},
    {"HTML", "<!DOCTYPE html"}
};

}
