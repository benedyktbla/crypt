
#include <string>

#include "crypt.h"
#include "../hill.h"

TESTCLASS(Affin,Crypt)

void AffinTest::test() {
    using namespace crypt;
    linalg::Matrix<zn::Z<256>,1,1> xx;
    xx = 7;

    S<AffinCipher, Padding> s;

    s.k = {1, 2};
    endecrypt(s, "abcdefg", "cdefghi");
    blind(s);

    s.k = {93, 18};
    blind(s);

    s.k = {12, 13};
    bad_key(s);
}

