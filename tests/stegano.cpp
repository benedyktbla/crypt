
#include <sstream>

#include "test.h"
#include "../stegano.h"

TESTCLASS(Stegano,)

void SteganoTest::test() {
    using namespace crypt::stegano;
    using namespace std;

    istringstream c("FF""DDDD""DDDD""DDDD""DDDD""DDDD""DDDD""F\xff""F");
    ostringstream o;
    stegano(c, "AB", o, 2);
    ch(o.str() == "FF""EDDDDDED""DEDDDDED""DDDDDDDD""F\xff""F");
    istringstream m(o.str());
    ostringstream oo;
    unstegano(m, oo, 2);
    ch(oo.str() == "AB");
}
