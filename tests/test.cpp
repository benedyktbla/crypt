
#include "test.h"
using namespace std;

Test::Test(string name) : name(name) {
    get_all_tests().push_back(this);
}

Test::~Test() { }

void Test::ok_() {
    tests_total++;
    tests_ok++;
    cerr << ".";
}

void Test::fail_(string file, int line, string expr) {
    tests_total++;
    tests_fail++;
    cerr << "F" << endl << name << ": ";
    if(file != "") cerr << file << ":" << line << ": ";
    if(expr != "") cerr << "`" << expr << "` ";
    cerr << "failed." << endl;
}

void Test::error_(exception &e, string file, int line, string expr) {
    tests_total++;
    tests_error++;
    cerr << "E" << endl << name << ": ";
    if(file != "") cerr << file << ":" << line << ": ";
    if(expr != "") cerr << "`" << expr << "` ";
    cerr << "exception thrown: " << e.what() << endl;
}

bool Test::run() {
    tests_total = 0;
    tests_ok = 0;
    tests_fail = 0;
    tests_error = 0;

    cerr << name << ": ";
    try {
        test();
    } catch(exception &e) {
        tests_error++;
        cerr << "X" << endl;
        cerr << "Exception thrown: " << e.what() << endl;
    } catch(...) {
        tests_error++;
        cerr << "X" << endl;
        cerr << "Exception thrown" << endl;
    }
    cerr << endl << "   " << tests_total << " total, " << tests_ok << " ok, "
        << tests_fail << " failed, " << tests_error << " exceptions." << endl;
    return tests_total == tests_ok;
}

std::vector<Test *> &Test::get_all_tests() {
    static vector<Test *> alltests;
    return alltests;
}

void Test::run_all() {
    bool ok = true;
    for(auto i = get_all_tests().begin(); i != get_all_tests().end(); i++) {
        cerr << "============" << endl;
        ok = (*i)->run() && ok;
    }
    cerr << "============" << endl;
    cerr << (ok ? "OK" : "Errors") << endl;
}

int main() {
    Test::run_all();
    return 0;
}

