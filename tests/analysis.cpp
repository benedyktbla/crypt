
#include "test.h"
#include "crypt.h"
#include "../hill.h"
#include "../padding.h"
#include "../analysis.h"

TESTCLASS(HillAnalysis,Crypt)

const unsigned char *r(const char *c) {
    return reinterpret_cast<const unsigned char *>(c);
}

void HillAnalysisTest::test() {
    using namespace crypt;
    S<HillCipher<1>, Padding> s1;
    s1.k = {93, 198};
    std::string i1 = "%PDF", o1 = s1.encrypt(i1);
    auto k1 = HillKnownPlaintext<1>().attack(i1.length(), r(o1.c_str()), r(i1.c_str()));
    ch(s1.k.a == k1.a && s1.k.b == k1.b);

    S<HillCipher<2>, Padding> s2;
    s2.k = {{13, 23, 22, 99}, {100, 77}};
    std::string i2 = "%PDFqazwsxedcrfvtgbyhn", o2 = s2.encrypt(i2);
    auto k2 = HillKnownPlaintext<2>().attack(i2.length(), r(o2.c_str()), r(i2.c_str()));
    ch(s2.k.a == k2.a && s2.k.b == k2.b);

    S<HillCipher<3>, Padding> s3;
    s3.k = {{13, 23, 22, 98, 23, 18, 19, 31, 7}, {100, 77, 22}};
    std::string i3 = "qazwsxedcrfvtgbyhnujmik,ol.p;/1234567890-=", o3 = s3.encrypt(i3);
    auto k3 = HillKnownPlaintext<3>().attack(i3.length(), r(o3.c_str()), r(i3.c_str()));
    ch(s3.k.a == k3.a && s3.k.b == k3.b);
}
