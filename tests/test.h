
#ifndef TEST_H
#define TEST_H

#include <string>
#include <vector>
#include <exception>
#include <iostream>

class Test {
public:
    Test(std::string name);
    virtual ~Test();
    bool run();
    static void run_all();
protected:
    void ok_();
    void fail_(std::string file, int line, std::string expr = "");
    void error_(std::exception &e, std::string file, int line, std::string expr = "");

    template<typename F>
    void ch_(F f, std::string expr, std::string file, int line) {
        try {
            if(f()) {
                ok_();
            } else {
                fail_(file, line, expr);
            }
        } catch(std::exception &e) {
            error_(e, file, line, expr);
        }
    }

    template<typename E, typename F>
    void throws_(F f, std::string expr, std::string file, int line) {
        try {
            f();
            fail_(file, line, expr);
        } catch(E &) {
            ok_();
        } catch(std::exception &e) {
            error_(e, file, line, expr);
        }
    }

    template<typename F>
    void pr_(F f, std::string expr, std::string file, int line) {
        std::cerr << file << ":" << line << ": " << expr << " = ";
        try {
            std::cerr << f() << std::endl;
        } catch(std::exception &e) {
            std::cerr << "<exception: " << e.what() << std::endl;
        }
    }

#define ok() ok_()
#define fail() fail_(__FILE__, __LINE__)
#define error(e) error_(e, __FILE__, __LINE__)

#define ch(x) ch_([&](){ return x; }, #x, __FILE__, __LINE__)
#define throws(x, E) throws_<E>([&](){ return x; }, #x, __FILE__, __LINE__)

#define pr(x) pr_([&](){ return x; }, #x, __FILE__, __LINE__)

    virtual void test() = 0;
private:
    std::string name;
    unsigned tests_total = 0, tests_ok = 0, tests_fail = 0, tests_error = 0;
    static std::vector<Test *> &get_all_tests();
};

#define TESTCLASS(Name,Base)  \
static class Name##Test : public Base##Test { \
public: \
    Name##Test() : Base##Test(#Name) { } \
    void test() override; \
} t; \

#endif

