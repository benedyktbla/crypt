
#include <stdexcept>
#include <string>
#include <sstream>

#include "test.h"
#include "../zn.h"

TESTCLASS(Zn,)

void ZnTest::test() {
    using namespace crypt::zn;
    typedef signed char schar;

    Z<7> s1 = schar(-2), s2 = schar(-100), s3 = schar(-128);
    ch(int(s1) == 5);
    ch(int(s2) == 5);
    ch(int(s3) == 5);

    Z<256> a0, a1 = 1, c = -1, a7 = 7, a255 = 255;
    Z<256> b7 = a7, a9 = char(9);
    ch(sizeof(a0) == 1);
    ch(int(a0) == 0);
    ch(int(a1) == 1);
    ch(int(a7) == 7);
    ch(int(a255) == 255);
    ch(int(c) == 255);
    ch(int(b7) == 7);
    ch(int(a9) == 9);
    Z<256> a65 = 'A', a127 = char(127), a128 = char(128);
    ch(a65 == 65);
    ch(a127 == 127);
    ch(a128 == 128);
    ch(a127 != a255);

    ch(a7 == b7);
    ch(a1 != a7);
    ch(!(a0 == a1));
    ch(!(a255 != c));
    ch(a255 != 127);

    Z<256> a16 = 16, a240 = 240, a3 = 3, a10 = 10, a21 = 21, a208 = 208, a192 = 192;
    ch(-a1 == c);
    ch(-c == a1);
    ch(-a240 == a16);
    ch(-a0 == a0);

    ch(a3 + a7 == a10);
    ch(a240 + a208 == a192);
    ch(a240 + a16 == a0);
    ch(a240 + a0 == a240);

    ch(a10 - a3 == a7);
    ch(a3 - a10 == -a7);
    ch(a0 - a208 == -a208);
    ch(a1 - a0 == a1);
    ch(a7 - a7 == a0);

    ch(a3 * a7 == a21);
    ch(a240 * a3 == a208);
    ch(a21 * a0 == a0);
    ch(a21 * a1 == a21);
    ch(a21 * a255 == -a21);
    ch(a7*(a192 + a21) == a7*a192 + a7*a21);

    Z<256> a61 = 61, a223 = 223;
    ch(a1.inv() == a1);
    ch(a255.inv() == a255);
    throws(a0.inv(), std::domain_error);
    ch(a21.inv() == a61);
    ch(a61.inv() == a21);
    ch(a223.inv() * a223 == a1);
    ch((-a61).inv() == -a21);

    ch(a3 / a21 == a3 * a61);
    ch(a208 / a255 == -a208);
    ch(a21 / a1 == a21);
    ch(a61 / a61 == a1);

    ch(std::string(a255) == "255");
    std::ostringstream os;
    os << a61;
    ch(os.str() == "61");

    //TODO = += -= *= /=

}

