
#include "test.h"
#include "../bigint.h"

TESTCLASS(BigInt,)

void BigIntTest::test() {
    using namespace crypt::bigint;
    UInteger b1("ffffffff ffffffff");
    UInteger b2("ffffffff ffffffff ffffffff");
    UInteger b3("ffffffff fffffffe ffffffff 00000000 00000001");
    ch(b1 * b2 == b3);
    ch(b3 / b2 == b1);
    ch(b1 <= b1);
    typedef UInteger8 B;
    ch(B("ffffff") * B("ffffffffffffff") == B("fffffeffffffff000001"));
    ch(B("fffffeffffffff000001") / B("ffffff") == B("ffffffffffffff"));
    ch(B("fffffeffffffff000001") % B("ffffff") == B());
    ch(B("fffffeffffffff000002") / B("ffffff") == B("ffffffffffffff"));
    ch(B("fffffeffffffff000002") % B("ffffff") == B("1"));
    ch(B("fffffeffffffff003001") / B("ffffff") == B("ffffffffffffff"));
    ch(B("fffffeffffffff003001") % B("ffffff") == B("3000"));
    ch((B("fedcba98765432111") * B("555577779") + B("555577778")) / B("555577779") == B("fedcba98765432111"));
    ch((B("fedcba98765432111") * B("555577779") + B("555577778")) % B("555577779") == B("555577778"));
    ch(B("ffffff") / B("ffffffffffffff") == B());
    ch(B("ffffff") % B("ffffffffffffff") == B("ffffff"));
    ch(B("878787") / B("878788") == B());
    ch(B("878787") % B("878788") == B("878787"));
    ch(B("878787") / B("878785") == B("1"));
    ch(B("878787") % B("878785") == B("2"));
    ch(B("ffffff") / B("22") == B("078787"));
    ch(B("ffffff") % B("22") == B("11"));
    ch(B("fefefefe") + B("99008801") == B("197ff86ff"));
    ch(B("ffffff") + B("100") == B("10000ff"));
    ch(B("100") + B("ffffff") == B("10000fF"));
    ch(B("10000ff") - B("100") == B("ffffff"));
    ch(B("fedcba") - B("fedcba") == B());
    ch(B("fedcba98765431") << 13 == B("1fdb97530eca862000"));
    ch(B("fedcba98765431") << 16 == B("fedcba987654310000"));
    ch(B("fedcba98765431") << 0 == B("fedcba98765431"));
    ch((B("fedcba98765431") <<= 0) == B("fedcba98765431"));
    ch(B("fedcba98765431") >> 13 == B("7f6e5d4c3b2"));
    ch(B("fedcba98765431") >> 16 == B("fedcba9876"));
    ch(B("fedcba98765431") >> 0 == B("fedcba98765431"));
    ch((B("fedcba98765431") >>= 0) == B("fedcba98765431"));
    ch(B() < B("1"));
    ch(!(B() < B()));
    ch(B("111111") < B("111112"));
    ch(!(B("111211") < B("111111")));
    ch(B() == B("000 00"));
    ch(B("2") != B("000 01"));
    ch(B("aaaaaaaa") != B("aaaaaabb"));

    throws(B("2") - B("9"), std::overflow_error);
    throws(B("2") - B("999999"), std::overflow_error);
    throws(B("1") / B(), std::domain_error);
}
