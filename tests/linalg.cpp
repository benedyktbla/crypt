
#include <stdexcept>
#include <sstream>

#include "test.h"
#include "../zn.h"
#include "../linalg.h"

TESTCLASS(LinAlg,)

void LinAlgTest::test() {
    using namespace crypt::zn;
    using namespace crypt::linalg;

    typedef DMatrix<Z<256>> D;
    typedef SMatrix<Z<256>, 2, 3> S23;
    typedef SMatrix<Z<256>, 3, 2> S32;
    typedef SMatrix<Z<256>, 3, 3> S33;
    typedef SMatrix<Z<256>, 2, 2> S22;
    typedef SMatrix<Z<256>, 1, 2> S12;

    int arr1[] {1,2,3,4,5,6};
    int arr2[] {1,2,3,4,5,6};
    Array1Matrix<int> a(2,3,arr1), b(2,3,arr2), c(3,2,arr1);
    ch(a.rows() == 2);
    ch(a.cols() == 3);
    ch(a(0,0) == 1);
    ch(a(0,1) == 2);
    ch(a(0,2) == 3);
    ch(a(1,0) == 4);
    ch(a(1,1) == 5);
    ch(a(1,2) == 6);
    ch(a == b);
    ch(a != c);
    ch(!(a != b));
    ch(!(a == c));
    a(1,2) = 7;
    ch(a(1,2) == 7);
    ch(a != b);
    arr2[5] = 7;
    ch(a == b);

    D da(2,3,{1,2,3,4,5,7});
    S23 sa{1,2,3,4,5,7};
    ch(da == a);
    ch(sa == a);

    D db(2,3,fill(9));
    S23 sb = fill(9);
    ch(db == S23({9,9,9,9,9,9}));
    ch(sb == D(2,3,{9,9,9,9,9,9}));
    ch(db == fill(9));
    ch(sb == fill(9));

    const D dc(sa);
    const S23 sc(da);
    ch(dc == da);
    ch(sc == sa);
    ch(da(1,2) == 7);
    ch(sa(1,2) == 7);
    ch(dc(1,2) == 7);
    ch(sc(1,2) == 7);

    da(1,2) = 8;
    sa(1,2) = 8;
    ch(da == D(2,3,{1,2,3,4,5,8}));
    ch(sa == S23({1,2,3,4,5,8}));

    D dd(2,3), de = D(2,3,{1,2,3,1,2,3});
    S23 sd, se = S23({1,2,3,1,2,3});
    ch(dd == S23({0,0,0,0,0,0}));
    ch(sd == S23({0,0,0,0,0,0}));
    ch(de == S23({1,2,3,1,2,3}));
    ch(se == S23({1,2,3,1,2,3}));

    db = da;
    sb = sa;
    ch(db == da);
    ch(sb == sa);

    db = fill(8);
    sb = fill(8);
    ch(db == S23({8,8,8,8,8,8}));
    ch(sb == S23({8,8,8,8,8,8}));

    db = sa;
    sb = da;
    ch(db == sa);
    ch(sb == da);

    db = D(2,3,{1,1,2,2,3,3});
    sb = S23({1,1,2,2,3,3});
    ch(db == sb);
    ch(db(1,1) == 3);
    ch(sb(1,1) == 3);



    S23 ma{1, 2, 3, 4, 5, 6}, mb{-9, 7, -5, 3, 1, -1}, mz;
    ch(255*ma == S23({-1, -2, -3, -4, -5, -6}));
    ch(ma*255 == S23({-1, -2, -3, -4, -5, -6}));
    ch(-ma == 255*ma);
    ch(2*ma == S23({2, 4, 6, 8, 10, 12}));
    ch(ma + mz == ma);
    ch(ma + mb == S23({-8, 9, -2, 7, 6, 5}));
    ch(mz - mb == -mb);
    ch(mb - mz == mb);
    ch(ma - ma == mz);
    ch(ma - mb == S23({10, -5, 8, 1, 4, 7}));
    ch(mb - ma == -(ma -  mb));

    S32 mt = transp(ma);
    ch(mt == S32({1, 4, 2, 5, 3, 6}));
    ch(mt * mb == S33({3, 11, -9,   -3, 19, -15,   -9, 27, -21}));
    ch((169*mb) * mt == 169*(mb*mt));

    S22 na{3, 4, 5, 9}, nz;
    ch(na * nz == nz);
    //ch(na.det() == 3*9 - 4*5);
    ch(inv(na) == (Z<256>(1)/7) * S22({9, -4, -5, 3}));
    throws(inv(nz), std::domain_error);

    S22 nb(id());
    ch(nb == S22({1, 0, 0, 1}));
    ch(id<Z<256>>(2) == id());
    ch(na * nb == na);
    ch(nb * ma == ma);
    ch(na * inv(na) == nb);
    ch(nb * nb == nb);
    ch(inv(nb) == nb);

    ch(to_string(nb) == "[ 1 0 ]\n[ 0 1 ]\n");
    std::ostringstream os;
    os << ma;
    ch(os.str() == "[ 1 2 3 ]\n[ 4 5 6 ]\n");

    S12 va = {1, 2};
    ch(va(0,0) == 1);
    ch(va(0,1) == 2);
    const S12 vc = va;
    ch(vc(0,0) == 1);
    ch(vc(0,1) == 2);
    ch(va * na == S12({13, 22}));

    ch(sizeof(ma) == 6);

    S23 x = {1, 2, 3, 4, 5, 6};
    D y(3, 2, {1, 3, 3, 5, 5, 7});
    ch(-transp(x * y + 13 * inv(13 * id<Z<256>>(2))) == S22({-23,-49,-34,-80}));

    //TODO += -= *= 1x1

}

