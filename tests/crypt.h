
#include <string>
#include <sstream>

#include "test.h"
#include "../crypt.h"

class CryptTest : public Test {
public:
    using Test::Test;
protected:

    template<typename C, typename P>
    struct S {
        C c;
        typename C::Key k;
        typename P::Pad pe;
        typename P::UnPad pd;

        std::string encrypt(std::string in) {
            std::istringstream is;
            std::ostringstream os;
            is.str(in);
            c.set_ekey(k);
            c.encrypt(is, os, pe);
            return os.str();
        }

        std::string decrypt(std::string in) {
            std::istringstream is;
            std::ostringstream os;
            is.str(in);
            c.set_dkey(k);
            c.decrypt(is, os, pd);
            return os.str();
        }
    };

    template<typename C, typename P>
    void endecrypt_(S<C, P> &s, std::string in, std::string out,
            std::string file, int line) {
        auto o = s.encrypt(in);
        ch_([&](){return o == out;}, "\"" + o + "\" == \"" + out + "\" /* en */", file, line);
        auto i = s.decrypt(o);
        ch_([&](){return i == in;}, "\"" + i + "\" == \"" + in + "\" /* de */", file, line);
    }
#define endecrypt(s,i,o) endecrypt_(s,i,o,__FILE__,__LINE__)

    template<typename C, typename P>
    void blind_(S<C, P> &s, std::string file, int line) {
        std::string samples[] = {"", "abcd", "abcde", "cqwdfhqiudhcqwdfqawf", "qwiudcqhwufqweiuifhqwdecqwqqc"};
        for(auto&& i : samples) {
            auto o = s.encrypt(i);
            auto ii = s.decrypt(o);
            //ch_([&](){return i == ii;}, "\"" + i + "\" == \"" + ii + "\"", file, line);
            ch_([&](){return i == ii;}, "\"" + i + "\" == [" + std::to_string(ii.length()) + "]\"" + ii + "\"", file, line);
        }
    }
#define blind(s) blind_(s,__FILE__,__LINE__)

    template<typename C, typename P>
    void bad_key_(S<C, P> &s, std::string file, int line) {
        throws_<crypt::Cipher::key_error>([&](){s.c.set_ekey(s.k);}, "c.set_ekey(k)", file, line);
        throws_<crypt::Cipher::key_error>([&](){s.c.set_dkey(s.k);}, "c.set_dkey(k)", file, line);
    }
#define bad_key(s) bad_key_(s,__FILE__,__LINE__)

};

