
#include "test.h"
#include "../util.h"

TESTCLASS(Util,)

void UtilTest::test() {
    using namespace crypt::util;
    /*typedef void (*fp)(long);
    ch((is_subscriptable<int*,int>::value));
    ch(!(is_subscriptable<int,int>::value));
    ch(!(is_callable<int,int>::value));
    ch((is_callable<fp,int>::value));*/
    const int n = 8;
    bool p[n];
    for(int i = 0; i < n; i++) p[i] = false;
    for(int i = 0; i < 1000; i++) {
        int x = random(n);
        if(x >= 0 && x < n) {
            p[x] = true;
        } else {
            fail();
        }
    }
    for(int i = 0; i < n; i++) {
        ch(p[i]);
    }
}
