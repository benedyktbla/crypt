
#include "test.h"
#include "crypt.h"
#include "../hill.h"
#include "../padding.h"

TESTCLASS(Hill,Crypt)

void HillTest::test() {
    using namespace crypt;
    S<HillCipher<2>, Padding> s0;
    S<HillCipher<2>, TwoPadding> s;

    s0.k = {{0, 1, 1, 0}, {1, 4}};
    endecrypt(s0, "abcdef", "ceeggi");

    s.k = {{93, 18, 123, 227}, {198, 19}};
    blind(s);

    s.k = {{198, 22, 206, 64}, {13, 11}};
    bad_key(s);
}
