
CXX ?= g++
CPPFLAGS += -MMD -MP
CXXFLAGS += -std=gnu++14 -Wall -Wextra -Wold-style-cast -Woverloaded-virtual -Wsign-promo
#-Wpedantic -Wsign-conversion -Wconversion -Wuninitialized -Winline
LDFLAGS +=
LDLIBS +=
RM ?= -rm -f
GCOVR ?= gcovr -k -r .

EXE = main
MAIN_SRCS = $(patsubst %,%.cpp,$(EXE))
SRCS = $(filter-out $(MAIN_SRCS),$(wildcard *.cpp))
OBJS = $(patsubst %.cpp,%.o,$(SRCS))

TESTDIR = tests
TEST = $(TESTDIR)/test
TEST_MAIN_SRCS = $(patsubst %,%.cpp,$(TEST))
TEST_SRCS = $(filter-out $(TEST_MAIN_SRCS),$(wildcard $(TESTDIR)/*.cpp))
TEST_OBJS = $(patsubst %.cpp,%.o,$(TEST_SRCS))

PERFDIR = perf
PERF = $(PERFDIR)/perf

.PHONY: all test testcov perf debug clean FORCE

.SUFFIXES:

.SECONDARY:

all: CPPFLAGS += -DNDEBUG
all: CXXFLAGS += -O3
all: $(EXE)

test: CXXFLAGS += -O3
test: $(TEST)
	$(foreach i,$(TEST),./$(i) && ) true

testcov: CXXFLAGS += --coverage
testcov: LDFLAGS += --coverage
testcov: $(TEST)
	$(foreach i,$(TEST),./$(i) && ) true
	$(GCOVR)

perf: CPPFLAGS += -DNDEBUG
perf: CXXFLAGS += -O3
perf: $(PERF)
	$(foreach i,$(PERF),./$(i) && ) true

debug: CXXFLAGS += -g -Og
debug: LDFLAGS += -g
debug: $(EXE) $(TEST)
	$(foreach i,$(TEST),./$(i) && ) true

%: %.o $(OBJS) _ldflags
	$(CXX) $(LDFLAGS) -o $@ $(filter %.o,$^) $(LDLIBS)

$(TESTDIR)/%: $(TESTDIR)/%.o $(TEST_OBJS) $(OBJS) _ldflags
	$(CXX) $(LDFLAGS) -o $@ $(filter %.o,$^) $(LDLIBS)

$(PERFDIR)/%: $(PERFDIR)/%.o $(OBJS) _ldflags
	$(CXX) $(LDFLAGS) -o $@ $(filter %.o,$^) $(LDLIBS)

%.o: %.cpp _cxxflags _cppflags
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c -o $@ $<

_cxxflags: FORCE
	@[ -e _cxxflags ] && [ "$$(cat _cxxflags)" = "$(CXXFLAGS)" ] || echo "$(CXXFLAGS)" >_cxxflags

_cppflags: FORCE
	@[ -e _cppflags ] && [ "$$(cat _cppflags)" = "$(CPPFLAGS)" ] || echo "$(CPPFLAGS)" >_cppflags

_ldflags: FORCE
	@[ -e _ldflags ] && [ "$$(cat _ldflags)" = "$(LDFLAGS)" ] || echo "$(LDFLAGS)" >_ldflags

clean:
	$(RM) $(EXE) $(TEST) $(PERF) \
		$(foreach i,d o gcov gcda gcno,*.$(i) $(TESTDIR)/*.$(i) $(PERFDIR)/*.$(i) ) \
		_cxxflags _cppflags _ldflags

FORCE:

-include *.d $(TESTDIR)/*.d $(PERFDIR)/*.d

