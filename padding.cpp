
#include "padding.h"

using namespace std;
namespace crypt {

void TwoPadding::Pad::set(unsigned block_size, std::istream &is) {
    if(block_size != 2)
        throw Padding::error("Wrong block size for this padding scheme");
    first = true;
    Padding::Pad::set(block_size, is);
}

TwoPadding::Pad &TwoPadding::Pad::operator++() {
    if(first) {
        first = false;
        if(stream_size % 2 == 0) {
            block.data()[0] = 128 + util::random(128);
            block.data()[1] = util::random(256);
        } else {
            block.data()[0] = util::random(128);
            block.data()[1] = read_byte();
        }
    } else {
        Padding::Pad::operator++();
    }
    return *this;
}

void TwoPadding::UnPad::set(unsigned block_size, std::ostream &os) {
    if(block_size != 2)
        throw Padding::error("Wrong block size for this padding scheme");
    first = true;
    Padding::UnPad::set(block_size, os);
}

TwoPadding::UnPad &TwoPadding::UnPad::operator++() {
    if(first) {
        first = false;
        if(unsigned(block.data()[0]) < 128) {
            write_byte(block.data()[1]);
        }
    } else {
        Padding::UnPad::operator++();
    }
    return *this;
}

}

