
#ifndef ANALYSIS_H
#define ANALYSIS_H

#include <string>
#include <map>
#include <algorithm>
#include <istream>
#include <memory>
//#include <iostream>

#include "hill.h"
#include "linalg.h"

namespace crypt {

class analysis_error : public crypt::crypt_error { using crypt::crypt_error::crypt_error; };

extern std::map<const std::string, const std::string> signatures;

template<unsigned n = 1>
class HillKnownPlaintext {
public:

    typename crypt::HillCipher<n>::Key attack(unsigned size, const unsigned char *ciphertext,
            const unsigned char *plaintext) {

        //for(unsigned i = 0; i < size; i++) {
        //    std::cerr << std::hex << int(ciphertext[i]) << " " << int(plaintext[i]) << std::endl << std::dec;
        //}

        //[x0 x1 1][a00 a01]   [y0 y1]
        //[x2 x3 1][a10 a11] = [y2 y3]
        //[x4 x5 1][ b0  b1]   [y4 y5]

        if(size / n < n + 1) {
            throw analysis_error("Can't compute this");
        }

        linalg::DMatrix<zn::Z<256>> c(size / n, n + 1), d(size / n, n);

        const unsigned char *cbyte = ciphertext, *pbyte = plaintext;
        for(unsigned i = 0; i < c.rows(); i++) {
            for(unsigned j = 0; j < n; j++) {
                c(i, j) = *pbyte++;
                d(i, j) = *cbyte++;
            }
            c(i, n) = 1;
        }

        linalg::gaussian_elimination(c, d);
        //std::cerr << c << std::endl << d;
        if(linalg::slice(c, n + 1, n + 1) != linalg::id()
                || linalg::slice(c, n + 1, 0, c.rows() - n - 1, n + 1) != linalg::fill(0)
                || linalg::slice(d, n + 1, 0, d.rows() - n - 1, n) != linalg::fill(0)) {
            throw analysis_error("Can't compute this");
        }

        typename crypt::HillCipher<n>::Key key;
        for(unsigned i = 0; i < n; i++) {
            for(unsigned j = 0; j < n; j++) {
                key.a(i, j) = d(i, j);
            }
            key.b(0, i) = d(n, i);
        }
        return key;
    }

    auto signature_attack(std::istream &is, std::string fileext) {
        std::transform(fileext.begin(), fileext.end(), fileext.begin(), ::toupper);
        unsigned plen = signatures[fileext].length(), cskip = (n == 1 ? 0 : n), clen = plen + cskip;
        const unsigned char *p = reinterpret_cast<const unsigned char *>(signatures[fileext].c_str());
        std::unique_ptr<char[]> ciphertext(new char[clen]);
        is.read(ciphertext.get(), clen);
        is.seekg(0);
        unsigned char *c = reinterpret_cast<unsigned char *>(ciphertext.get());
        // 2 0
        // 1 0 -> 2 1
        for(unsigned i = 0; i < n; i++) {
            try {
                //std::cerr << "attack " << i << std::endl;
                return attack(plen - i, c + cskip, p + i);
            } catch(analysis_error &) { }
        }
        throw analysis_error("Can't compute this");
    }

};

}

#endif
