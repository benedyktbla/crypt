
#ifndef CRYPT_H
#define CRYPT_H

#include <istream>
#include <ostream>
#include <stdexcept>
#include <string>

#include "util.h"

namespace crypt {

class crypt_error : public std::logic_error { using std::logic_error::logic_error; };

class Padding {
public:
    class error : public crypt_error { using crypt_error::crypt_error; };

    class Pad : public util::InBlockIterator {
    public:
        void set(unsigned block_size, std::istream &is) override;
    protected:
        long stream_size;
    };

    class UnPad : public util::OutBlockIterator { };
};

class Cipher {
public:
    class error : public crypt_error { using crypt_error::crypt_error; };
    class key_error : public error { using error::error; };
    Cipher(unsigned block_size);
    virtual ~Cipher();
    unsigned get_block_size();
    void encrypt(std::istream &in, std::ostream &out, Padding::Pad &pad);
    void decrypt(std::istream &in, std::ostream &out, Padding::UnPad &unpad);
protected:
    unsigned block_size;
    virtual void encrypt_block(const unsigned char *inblk, unsigned char *outblk) = 0;
    virtual void decrypt_block(const unsigned char *inblk, unsigned char *outblk) = 0;
};

}

#endif
