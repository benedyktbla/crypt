
#include <iostream>
#include <fstream>
#include <string>

#include "zn.h"
#include "linalg.h"
#include "hill.h"
#include "padding.h"
#include "analysis.h"
#include "stegano.h"

using namespace std;
using namespace crypt;
using namespace crypt::zn;
using namespace crypt::linalg;
using namespace crypt::stegano;

void read_line(string msg, string &line) {
    cout << msg << flush;
    line = "";
    while(line.length() == 0)
        getline(cin, line);
}

char read_char(string msg) {
    string line;
    read_line(msg, line);
    return line[0];
}

int read_int(string msg) {
    cout << msg << flush;
    int i;
    cin >> i;
    return i;
}

template<unsigned m, unsigned n>
Matrix<Z<256>, m, n> read_matrix(string msg) {
    cout << msg << endl << flush;
    Matrix<Z<256>, m, n> a;
    for(unsigned i = 0; i < m; i++) {
        for(unsigned j = 0; j < n; j++) {
            a(i, j) = read_int(" [" + to_string(i) + "," + to_string(j) + "]=");
        }
    }
    return a;
}

void read_affin_key(AffinCipher::Key &k) {
    k.a = read_int("klucz a=");
    k.b = read_int("      b=");
}

void read_hill_key(HillCipher<2>::Key &k) {
    k.a = read_matrix<2,2>("klucz a:");
    k.b = read_matrix<1,2>("b:");
}

template<typename C, typename P, void (*read_key)(typename C::Key&)>
void do_crypt() {
    string name_in, name_out;
    read_line("plik wejsciowy: ", name_in);
    read_line("plik wyjsciowy: ", name_out);
    bool decrypt = read_char("szyfrowanie [s] / deszyfrowanie [d]? ") == 'd';
    ifstream in(name_in.c_str(), ios::binary);
    ofstream out(name_out.c_str(), ios::binary);
    C ciph;
    typename C::Key k;
    while(true) {
        read_key(k);
        try {
            if(decrypt) {
                ciph.set_dkey(k);
            } else {
                ciph.set_ekey(k);
            }
        } catch(Cipher::key_error &) {
            cout << "bledny klucz" << endl;
            if(read_char("jeszcze raz? [t/n]: ") != 't') {
                break;
            } else {
                continue;
            }
        }
        if(decrypt) {
            typename P::UnPad pd;
            ciph.decrypt(in, out, pd);
        } else {
            typename P::Pad pe;
            ciph.encrypt(in, out, pe);
        }
        cout << "ok" << endl;
        break;
    }
}

template<typename A, typename C, typename P>
void do_analysis() {
    string name_in, ext;
    read_line("plik zaszyfrowany: ", name_in);
    read_line("typ pliku: ", ext);
    ifstream in(name_in.c_str(), ios::binary);
    try {
        C c;
        c.set_dkey(A().signature_attack(in, ext));
        string name_out;
        read_line("plik wyjsciowy: ", name_out);
        ofstream out(name_out.c_str(), ios::binary);
        typename P::UnPad p;
        c.decrypt(in, out, p);
    } catch(crypt_error &e) {
        cout << "Nieudane deszyfrowanie" << endl;
    }
}

int main() {

    while(true) {
        cout << "[e] rozszerzony euklides" << endl;
        cout << "[i] odwrotnosc" << endl;
        cout << "[a] szyfr afiniczny" << endl;
        cout << "[h] szyfr Hilla(2)" << endl;
        cout << "[A] kryptooanaliza szyfru afinicznego" << endl;
        cout << "[H] kryptooanaliza szyfru Hilla(2)" << endl;
        cout << "[s] steganografia" << endl;
        cout << "[S] de-steganografia" << endl;
        cout << "[z] wyjscie" << endl;
        char c = read_char("wybierz: ");

        if(c == 'z') {
            break;
        } else if(c == 'e') {
            int a = read_int("a=");
            int b = read_int("b=");
            int u, v;
            int d = ext_euclid(a, b, u, v);
            cout << d << " = " << u << "*" << a << " + " << v << "*" << b << " = " << u*a+v*b << endl;
        } else if(c == 'i') {
            Z<256> a = read_int("a="), i;
            i = 1 / a;
            cout << "inv(" << a << ") = " << i << " in Z<256>" << endl;
        } else if(c == 'a') {
            do_crypt<AffinCipher, Padding, read_affin_key>();
        } else if(c == 'h') {
            do_crypt<HillCipher<2>, TwoPadding, read_hill_key>();
        } else if(c == 'A') {
            do_analysis<HillKnownPlaintext<1>, AffinCipher, Padding>();
        } else if(c == 'H') {
            do_analysis<HillKnownPlaintext<2>, HillCipher<2>, TwoPadding>();
        } else if(c == 's') {
            string name_in, name_out, payload;
            read_line("plik wejsciowy: ", name_in);
            read_line("plik wyjsciowy: ", name_out);
            int offset = read_int("offset: ");
            read_line("wiadomosc: ", payload);
            ifstream in(name_in.c_str(), ios::binary);
            ofstream out(name_out.c_str(), ios::binary);
            stegano::stegano(in, payload, out, offset);
        } else if(c == 'S') {
            string name_in;
            read_line("plik wejsciowy: ", name_in);
            int offset = read_int("offset: ");
            ifstream in(name_in.c_str(), ios::binary);
            unstegano(in, std::cout, offset);
            std::cout << endl;
        }
        cout << endl;
    }

    return 0;
}
