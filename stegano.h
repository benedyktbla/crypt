
#include <string>
#include <istream>
#include <ostream>

#include "crypt.h"

namespace crypt {
namespace stegano {

class stegano_error : public crypt::crypt_error { using crypt::crypt_error::crypt_error; };

void stegano(std::istream &carrier, std::string payload, std::ostream &msg,
        unsigned offset = 0);

void unstegano(std::istream &msg, std::ostream &os, unsigned offset = 0);

}
}
