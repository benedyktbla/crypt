
#include <string>
#include <iostream>
#include <ctime>

#include "../bigint.h"


template<typename F>
void perf_(std::string name, F f, int n) {
    std::clock_t t0 = std::clock();
    for(int i = 0; i < n; i++) {
        f();
    }
    std::clock_t t1 = std::clock();
    std::cerr << name << ": " << (1.0*(t1 - t0) / CLOCKS_PER_SEC) << "s" << std::endl;
}

#define perf(s,f,n) perf_(s, [&](){ return f; }, n)


void perf_bigint() {
    std::default_random_engine r;
#define ba(d,s,n) { \
    crypt::bigint::UInteger##d a(s, r), b(s, r); \
    perf("bigint" #d " add " #s "bits (x" #n ")", a + b, n); \
    perf("bigint" #d " add= " #s "bits (x" #n ")", a += b, n); \
}
    //ba(32, 16*1024, 1000000);
    //ba(32I, 16*1024, 1000000);
    ba(64, 16*1024, 1000000);
    //ba(64I, 16*1024, 1000000);
    ba(64, 4*1024, 10000000);
    //ba(64I, 4*1024, 10000000);
#define bm(d,s,n) { \
    crypt::bigint::UInteger##d a(s, r), b(s, r); \
    perf("bigint" #d " mul " #s "bits (x" #n ")", a * b, n); \
}
    //bm(8, 16*1024, 1000)
    //bm(16, 16*1024, 1000)
    //bm(32, 16*1024, 10000)
    //bm(32I, 16*1024, 10000)
    bm(64, 16*1024, 10000)
    //bm(64I, 16*1024, 10000)
    bm(64, 4*1024, 1000000)
    //bm(64I, 4*1024, 1000000)
#define bd(d,s,n) { \
    crypt::bigint::UInteger##d a(s*2, r), b(s, r); \
    perf("bigint" #d " div " #s "bits (x" #n ")", a / b, n); \
}
    //bd(8, 16*1024, 1000)
    //bd(16, 16*1024, 1000)
    //bd(32, 16*1024, 10000)
    //bd(32I, 16*1024, 10000)
    bd(64, 16*1024, 10000)
    //bd(64I, 16*1024, 10000)
    bd(64, 4*1024, 1000000)
    //bd(64I, 4*1024, 1000000)
}

int main() {
    perf_bigint();
    return 0;
}

